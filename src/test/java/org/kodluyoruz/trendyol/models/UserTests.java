package org.kodluyoruz.trendyol.models;

import comgrup2.urlshortener.redirector.models.Link;
import comgrup2.urlshortener.redirector.models.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserTests {

    User sut;
    List<Link> links;
    Link link1;
    Link link2;
    Link link3;
    List<String> tags1;
    List<String> tags2;
    List<String> tags3;
    String tag1;
    String tag2;
    String tag3;
    String tag4;
    String tag5;

    @BeforeEach
    public void setup() {
        links = new ArrayList<>();

        tag1 = "education";
        tag2 = "school";
        tag3 = "job";
        tag4 = "old";
        tag5 = "new";

        tags1 = new ArrayList<>();
        tags1.add(tag1);
        tags1.add(tag5);

        tags2 = new ArrayList<>();
        tags2.add(tag2);
        tags2.add(tag4);

        tags3 = new ArrayList<>();
        tags3.add(tag1);
        tags3.add(tag2);
        tags3.add(tag5);

        link1 = new Link("1a45cca","www.google.com", "123456",null, "first link",tags1,0);
        link2 = new Link("14dcs44","www.tredyol.com", "",null, "second link",tags2,5);
        link3 = new Link("csac45a","www.gtu.edu.tr", "111111",null, "third link",tags3,10);
    }

    @AfterEach
    public void cleanup() {
        sut = null;
    }

    @Test
    public void increase1UsageCounter_WhenIDIsExistAndLinkUsageCounterIsZero_ShouldIncrease1UsageCounterOfGivenLinkAndMakesUsageCounterOne() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        //Act
        sut.increase1UsageCounter(link1.getId());

        //Assert
        assertThat(sut.getLinks().get(0).getUsageCounter()).isEqualTo(1);
    }

    @Test
    public void increase1UsageCounter_WhenIDIsExistAndLinkUsageCounterIsFive_ShouldIncrease1UsageCounterOfGivenLinkAndMakesUsageCounterSix() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        //Act
        sut.increase1UsageCounter(link2.getId());

        //Assert
        assertThat(sut.getLinks().get(1).getUsageCounter()).isEqualTo(6);
    }

    @Test
    public void increase1UsageCounter_WhenIDIsNotExist_ShouldNotIncrease1UsageCounterOfOtherLinks() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        //Act
        sut.increase1UsageCounter("ignoreId");

        //Assert
        assertThat(sut.getLinks().get(0).getUsageCounter()).isEqualTo(0);
        assertThat(sut.getLinks().get(1).getUsageCounter()).isEqualTo(5);
        assertThat(sut.getLinks().get(2).getUsageCounter()).isEqualTo(10);
    }

    @Test
    public void increase1UsageCounter_WhenIDIsExist_ShouldIncrease1UsageCounterOfGivenLink_ShouldNotIncrease1UsageCounterOfOtherLinks() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        //Act
        sut.increase1UsageCounter(link3.getId());

        //Assert
        assertThat(sut.getLinks().get(0).getUsageCounter()).isEqualTo(0);
        assertThat(sut.getLinks().get(1).getUsageCounter()).isEqualTo(5);
        assertThat(sut.getLinks().get(2).getUsageCounter()).isEqualTo(11);
    }
}
