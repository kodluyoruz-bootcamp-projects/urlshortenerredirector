package comgrup2.urlshortener.redirector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlshortenerredirectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(UrlshortenerredirectorApplication.class, args);
    }

}
