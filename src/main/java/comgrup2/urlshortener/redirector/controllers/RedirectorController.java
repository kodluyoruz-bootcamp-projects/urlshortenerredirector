package comgrup2.urlshortener.redirector.controllers;

import comgrup2.urlshortener.redirector.exceptions.IdNotFoundException;
import comgrup2.urlshortener.redirector.exceptions.InvalidIdException;
import comgrup2.urlshortener.redirector.models.Link;
import comgrup2.urlshortener.redirector.services.URLRedirectorService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.net.URI;
import java.util.List;

@RequestMapping("/")
@RestController
@CrossOrigin(origins="*", allowedHeaders = "*")
public class RedirectorController {

    URLRedirectorService urlRedirectorService;

    public RedirectorController(URLRedirectorService urlRedirectorService) {
        this.urlRedirectorService = urlRedirectorService;
    }

    @GetMapping
    public RedirectView home() {
        return new RedirectView("swagger-ui.html");
    }

    @GetMapping("/{linkId}")
    public ResponseEntity<Void> redirect(@PathVariable String linkId) {
        if (linkId.contains("*") || linkId.contains(".") || linkId.contains("/")) {
            throw new InvalidIdException();
        }

        try{
            Link link = this.urlRedirectorService.findTargetLinkByLinkId(linkId);
            increase1LinkUsageCounter(linkId);
            URI uri = new URI(link.getTargetLink());
            if(uri.getScheme() == null){
                uri = new URI("http://" + link.getTargetLink());
            }
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setLocation(uri);
            return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
        }catch (NullPointerException ex){
            throw new IdNotFoundException();
        } catch (Exception ex){
            throw new IdNotFoundException();
        }
    }

    private void increase1LinkUsageCounter(String linkId) {
        new Thread(new Runnable() {
            public void run() {
                urlRedirectorService.increase1LinkUsageCounter(linkId);
            }
        }).start();
    }

}
