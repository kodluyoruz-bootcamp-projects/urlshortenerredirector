package comgrup2.urlshortener.redirector.repositories;


import comgrup2.urlshortener.redirector.models.Link;
import comgrup2.urlshortener.redirector.models.User;
import org.springframework.stereotype.Repository;

@Repository
public interface URLRedirectorRepository {
    Link findTargetLinkByLinkId(String linkId);
    User findUserByLinkId(String linkId);
    void updateUser(User user);
}
