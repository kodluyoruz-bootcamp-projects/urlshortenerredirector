package comgrup2.urlshortener.redirector.repositories;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import comgrup2.urlshortener.redirector.models.User;
import com.couchbase.client.java.query.QueryResult;
import comgrup2.urlshortener.redirector.models.Link;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class URLRedirectorCouchBaseRepository implements URLRedirectorRepository{

    private final Cluster couchbaseCluster;
    private final Collection userCollection;

    public URLRedirectorCouchBaseRepository(Cluster couchbaseCluster, Collection userCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.userCollection = userCollection;
    }

    @Override
    public Link findTargetLinkByLinkId(String linkId) {
        String statement;
        statement = "SELECT lnk.targetLink FROM group2user UNNEST links lnk WHERE lnk.id == '" + linkId + "';";
        QueryResult query = this.couchbaseCluster.query(statement);
        List<Link> links = query.rowsAs(Link.class);

        return links.get(0);
    }

    @Override
    public User findUserByLinkId(String linkId) {
        String statement;
        statement = "SELECT group2user.email, group2user.id, group2user.linkCount, group2user.links, group2user.name, group2user.token FROM group2user UNNEST links lnk WHERE lnk.id == '" + linkId + "';";
        QueryResult query = this.couchbaseCluster.query(statement);
        List<User> users = query.rowsAs(User.class);

        return users.get(0);
    }

    @Override
    public void updateUser(User user) {
        userCollection.replace(user.getId(), user);
    }
}
