package comgrup2.urlshortener.redirector.services;

import comgrup2.urlshortener.redirector.models.Link;
import comgrup2.urlshortener.redirector.models.User;
import comgrup2.urlshortener.redirector.repositories.URLRedirectorCouchBaseRepository;
import comgrup2.urlshortener.redirector.repositories.URLRedirectorRepository;
import org.springframework.stereotype.Service;

@Service
public class URLRedirectorService {

    private final URLRedirectorRepository urlRedirectorRepository;

    public URLRedirectorService(URLRedirectorCouchBaseRepository urlRedirectorCouchBaseRepository) {
        this.urlRedirectorRepository = urlRedirectorCouchBaseRepository;
    }

    public Link findTargetLinkByLinkId(String linkId) {
        return this.urlRedirectorRepository.findTargetLinkByLinkId(linkId);
    }

    public void increase1LinkUsageCounter(String linkId) {
        User user = this.urlRedirectorRepository.findUserByLinkId(linkId);
        user.increase1UsageCounter(linkId);
        this.urlRedirectorRepository.updateUser(user);
    }
}
