package comgrup2.urlshortener.redirector.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String id;
    private String name;
    private String email;
    private String token;
    private List<Link> links;
    private int linkCount;

    public void increase1UsageCounter(String linkId){
        for (int i = 0; i < this.getLinkCount(); i++) {
            if(this.getLinks().get(i).getId().equals(linkId)){
                this.getLinks().get(i).setUsageCounter(this.getLinks().get(i).getUsageCounter() + 1);
                break;
            }
        }
    }
}
