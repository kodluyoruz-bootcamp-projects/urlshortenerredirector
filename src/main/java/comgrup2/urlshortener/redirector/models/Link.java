package comgrup2.urlshortener.redirector.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Link {
    private String id;
    private String targetLink;
    private String password;
    private Date expireDate;
    private String description;
    private List<String> tags;
    private int usageCounter;
}
